-- 3 --

CREATE TABLE user
(
  id       BIGINT                               NOT NULL PRIMARY KEY AUTO_INCREMENT UNIQUE,
  login    VARCHAR(30)                          NOT NULL UNIQUE,
  password CHAR(30)                             NOT NULL,

  type     ENUM ('plain', 'admin')              NOT NULL,
  status   ENUM ('active', 'banned', 'removed') NOT NULL,

  info_id  BIGINT
);

CREATE TABLE user_info
(
  user_id           BIGINT NOT NULL UNIQUE,
  first_name        VARCHAR(20),
  last_name         VARCHAR(20),
  registration_date DATE   NOT NULL,
  icon_reference    VARCHAR(255)
);


ALTER TABLE user
  ADD CONSTRAINT info_id_foreign_key
    FOREIGN KEY id (info_id) REFERENCES user_info (id);


CREATE TABLE plain_user
(
  user_id         BIGINT NOT NULL UNIQUE,
  balance         BIGINT NOT NULL,

  status          ENUM ('active', 'blocked'),

  tariff_id       BIGINT,
  bill_history_id BIGINT
);

CREATE TABLE tariff
(
  id                 BIGINT NOT NULL PRIMARY KEY UNIQUE,
  name               VARCHAR(30),
  description        VARCHAR(1024),
  free_mb_per_month  INT    NOT NULL,
  speed              INT    NOT NULL,
  speed_over_traffic INT,
  day_bill           BIGINT NOT NULL,

  status             ENUM ('active', 'no_new_users', 'removed')
);


CREATE TABLE bill_history
(
  id        BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  tariff_id BIGINT,
  sum       BIGINT             NOT NULL,
  date      DATE
);

CREATE TABLE discount
(
  id          BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  tariff_id   BIGINT             NOT NULL,
  start_date  DATE,
  finish_date DATE,
  percent     FLOAT
);


CREATE TABLE discount_user
(
  discount_id   BIGINT NOT NULL,
  plain_user_id BIGINT NOT NULL
);


ALTER TABLE discount_user
  ADD CONSTRAINT discount_id_foreign_key
    FOREIGN KEY id (discount_id) REFERENCES discount (id),
  ADD CONSTRAINT plain_user_id_foreign_key
    FOREIGN KEY id (user_id) REFERENCES client_user (user_id);


ALTER TABLE discount
  ADD CONSTRAINT tariff_id_foreign_key__
    FOREIGN KEY id (tariff_id) REFERENCES tariff (id);

ALTER TABLE client_user
  ADD CONSTRAINT user_foreign_key
    FOREIGN KEY id (user_id) REFERENCES user (id);

ALTER TABLE client_user
  ADD CONSTRAINT tariff_id_foreign_key
    FOREIGN KEY id (tariff_id) REFERENCES tariff (id),
  ADD CONSTRAINT bill_history_id_foreign_key
    FOREIGN KEY id (bill_history_id) REFERENCES bill_history (id);

ALTER TABLE bill_history
  ADD CONSTRAINT tariff_id_foreign_key_
    FOREIGN KEY id (tariff_id) REFERENCES tariff (id);