-- 2 --

USE provider_db;

-- provider admin responsible for adding data to tables
CREATE USER 'provider_admin'@'localhost' IDENTIFIED BY 'qwerty1';

GRANT SELECT, INSERT, UPDATE
  ON provider_db.* TO 'provider_admin'@'localhost';

-- provider root responsible for grant permissions and creating and
-- changing tables
CREATE USER 'provider_root'@'localhost' IDENTIFIED by 'qwerty1';

GRANT CREATE, ALTER, DROP ON provider_db.* TO 'provider_root'@'localhost';
