-- 5 --

USE provider_db;

DROP TABLE bill_history;
DROP TABLE discount;
DROP TABLE discount_user;
DROP TABLE client_user;
DROP TABLE tariff;
DROP TABLE user;
DROP TABLE user_info;
